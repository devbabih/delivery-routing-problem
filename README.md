# DeliveryRouting

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.4.

## Instalation

`npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

If you do not have global `ng`, do `./node_modules/.bin/ng serve`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
