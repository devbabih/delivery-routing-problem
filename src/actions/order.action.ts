import {Action} from '@ngrx/store';
import {OrderModel} from '../models/order.model';

export const LOAD_ORDERS = '[Orders] load';
export const LOAD_ORDERS_SUCCESS = '[Orders] load success';

export const DRAW_PATH = '[Orders] draw path';
export const DRAW_PATH_SUCCESS = '[Orders] draw path success';

export class LoadOrdersAction implements Action {
  readonly type = LOAD_ORDERS;
}

export class LoadOrdersSuccessAction implements Action {
  readonly type = LOAD_ORDERS_SUCCESS;

  constructor(public payload: OrderModel[]) {

  }
}

export class DrawPathAction implements Action {
  readonly type = DRAW_PATH;
}

export class DrawPathSuccessAction implements Action {
  readonly type = DRAW_PATH_SUCCESS;

  constructor(public payload: any) {
  }
}

export type Actions =
  LoadOrdersAction
    | LoadOrdersSuccessAction
    | DrawPathAction
    | DrawPathSuccessAction;
