import {Component, ViewChild, ElementRef, OnDestroy, OnInit} from '@angular/core';
import * as fromRoot from '../reducers/index';
import * as orderAction from '../actions/order.action';
import {Store} from '@ngrx/store';
import {MapService} from '../services/map.service';
import {ObservableMedia} from '@angular/flex-layout';
import {OrderModel} from '../models/order.model';
import {Observable, Subscription} from 'rxjs';
import {Http} from '@angular/http';

declare var google: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('map') mapEl: ElementRef;
  @ViewChild('sidenav') sidenav;
  @ViewChild('sidenavMob') sidenavMob;

  $loading: Observable<boolean>;
  $paths: Observable<any>;
  $orders: Observable<OrderModel[]>;

  pathS: Subscription;
  ordersS: Subscription;

  gMap: any;

  dataLoaded: boolean = false;

  constructor(private store: Store<fromRoot.AppState>, private mapService: MapService, public media: ObservableMedia, private http: Http) {

    this.$loading = this.store.select(fromRoot.getOrdersLoading);
    this.$paths = this.store.select(fromRoot.getPaths);
    this.$orders = this.store.select(fromRoot.getOrders).filter(o => o.length > 0);

    this.drawMarkers();
    this.generateRoutes();

  }


  ngOnInit() {
    this.mapService.getMap().subscribe(() => {
      let latlng = new google.maps.LatLng(30.699274102296, 76.809466440475);

      this.gMap = new google.maps.Map(this.mapEl.nativeElement, {
        center: latlng,
        zoom: 12
      });
    });
  }

  ngOnDestroy() {
    if (this.pathS) {
      this.pathS.unsubscribe();
    }

    if (this.ordersS) {
      this.ordersS.unsubscribe();
    }
  }

  loadData() {
    if (this.gMap) {
      this.store.dispatch(new orderAction.LoadOrdersAction());
    }
  }

  drawPaths() {
    if (this.dataLoaded) {
      this.store.dispatch(new orderAction.DrawPathAction());
    }
  }

  toggle() {
    if (this.sidenav) {
      this.sidenav.toggle();
    } else if (this.sidenavMob) {
      this.sidenavMob.toggle();
    }
  }


  private drawMarkers() {
    this.ordersS = this.$orders.subscribe((orders: OrderModel[]) => {
      if (orders.length > 0) {
        this.dataLoaded = true;
      }
      orders.forEach(order => {
        let latlng = new google.maps.LatLng(order.delivery_latitude, order.delivery_longitude);
        new google.maps.Marker({
          position: latlng,
          map: this.gMap,
          title: 'Hello World!'
        });
      });
    });
  }

  private generateRoutes() {
    this.pathS = this.$paths.filter(r => r).subscribe(r => {
      r.forEach(dataToPlot => {

        var service = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(this.gMap);

        var waypts = [];
        for (let j = 1; j < dataToPlot.length - 1; j++) {
          waypts.push({
            location: dataToPlot[j],
            stopover: true
          });
        }

        var request = {
          origin: dataToPlot[0],
          destination: dataToPlot[dataToPlot.length - 1],
          waypoints: waypts,
          travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        service.route(request, function (result, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
          }
        });

      });
    });
  }
}
