import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {AppComponent} from './app.component';
import {reducer} from '../reducers/index';
import {OrderEffects} from '../effects/order.effect';
import {OrderService} from '../services/order.service';
import {MapService} from '../services/map.service';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MdIconModule, MdToolbarModule, MdSidenavModule, MdListModule, MdProgressSpinnerModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdSidenavModule,
    MdToolbarModule,
    MdIconModule,
    MdListModule,
    MdProgressSpinnerModule,
    FlexLayoutModule,
    StoreModule.provideStore(reducer),
    EffectsModule.run(OrderEffects)
  ],
  providers: [OrderService, MapService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
