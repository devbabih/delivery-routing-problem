import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {OrderService} from '../services/order.service';
import {OrderModel} from '../models/order.model';
import * as orderAction from '../actions/order.action';
import * as fromRoot from '../reducers/';

@Injectable()
export class OrderEffects {

  @Effect()
  loadHomeDraft: Observable<Action> = this.actions$
    .ofType(orderAction.LOAD_ORDERS)
    .switchMap(() =>
      this.orderService.fetchOrders().map((orders: OrderModel[]) => {
        return new orderAction.LoadOrdersSuccessAction(orders);
      }).catch(error => Observable.empty()) //TODO handle error
    );

  @Effect()
  drawRoutes$: Observable<Action> = this.actions$
    .ofType(orderAction.DRAW_PATH)
    .withLatestFrom(this.store.select(fromRoot.getOrders))
    .map(([action, storeState]) => {
          return storeState;
      })
    .switchMap((orders) => {

        const service = orders
          .map(order => {
            return {
              "id": String(order.order_id),
              "lat": order.delivery_latitude,
              "lng": order.delivery_longitude,
              "name": String(order.order_id),
              "duration": 5
            }
          });

        return this.orderService.routeOptimization(service).map(resp => {
          //TODO - NOTICE polylines was providing wrong information such as lat 307 and lng 700 ..... so I had to create workaround
          var solutions = resp.json().data.solutions;

          var result = [];
          Object.keys(solutions).map((objectKey) => {
            var solution = solutions[objectKey];
            var dataToPlot = [];

            solution.forEach(point => {
              let order = orders.find((el) => {
                return el.order_id == point.id;
              });

              if (order) {
                dataToPlot.push({lat: order.delivery_latitude, lng: order.delivery_longitude});
              }
            });

            result.push(dataToPlot);
          });
          return new orderAction.DrawPathSuccessAction(result);
        }).catch(error => Observable.empty()); //TODO handle error
      }
    );


  constructor(private store: Store<fromRoot.AppState>, private actions$: Actions, private orderService: OrderService) {
  }
}
