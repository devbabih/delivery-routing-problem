export interface OrderModel {
  delivery_latitude: number,
  delivery_longitude: number,
  ignore: number,
  order_id: number,
  delivery_address: string,
  delivery_date: string,
  delivery_slot_id: number
}
