import * as fromOrder from './order.reducer';
import {ActionReducer, combineReducers} from '@ngrx/store';
import {storeFreeze} from 'ngrx-store-freeze';
import {compose} from '@ngrx/core';
import {createSelector} from 'reselect';
import {environment} from '../environments/environment';

export interface AppState {
  orders: fromOrder.State
}

const reducers = {
  orders: fromOrder.reducer
};

const developmentReducer: ActionReducer<AppState> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<AppState> = combineReducers(reducers);

export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

export const getOrderState = (state: AppState) => state.orders;

export const getOrders = createSelector(getOrderState, fromOrder.getOrders);
export const getOrdersLoading = createSelector(getOrderState, fromOrder.getOrdersLoading);
export const getPaths = createSelector(getOrderState, fromOrder.getPaths);
