import {OrderModel} from '../models/order.model';
import * as orderActions from '../actions/order.action';


export interface State {
  orders: OrderModel[];
  paths: any;
  loading: boolean;
}

export const initialState: State = {
  orders: [],
  loading: false,
  paths: undefined
};

export function reducer(state = initialState, action: orderActions.Actions): State {
  switch (action.type) {
    case orderActions.LOAD_ORDERS: {
      return Object.assign({}, state, {
        loading: true
      });
    }
    case orderActions.LOAD_ORDERS_SUCCESS: {
      let orders: OrderModel[] = action.payload;
      return Object.assign({}, state, {
        orders: orders,
        loading: false
      });
    }
    case orderActions.DRAW_PATH: {
      return Object.assign({}, state, {
        loading: true
      });
    }
    case orderActions.DRAW_PATH_SUCCESS: {
      let paths = action.payload;
      return Object.assign({}, state, {
        paths: paths,
        loading: false
      });
    }
    default: {
      return state;
    }
  }
}
export const getOrders = (state: State) => state.orders;
export const getOrdersLoading = (state: State) => state.loading;
export const getPaths = (state: State) => state.paths;
