import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';

const MAP_URL = 'https://maps.googleapis.com/maps/api/js?libraries=geometry&key=' + environment.google_map_key + '&callback=initMap';

@Injectable()
export class MapService {

  private mapLoad: Observable<any>;

  constructor() {
    this.mapLoad = new Observable((observer) => {
      window['initMap'] = () => {
        observer.next();
      };
      this.loadScript()
    });
  }

  public getMap(): Observable<any> {
    return this.mapLoad;
  }

  private loadScript() {
    let script = document.createElement('script');
    script.src = MAP_URL;
    script.type = 'text/javascript';

    if (document.body.contains(script)) {
      return;
    }
    document.getElementsByTagName('head')[0].appendChild(script);
  }
}
