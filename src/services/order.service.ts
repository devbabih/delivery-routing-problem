import {Injectable} from '@angular/core';
import {RequestOptions, Http, RequestOptionsArgs, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import {OrderModel} from '../models/order.model';

@Injectable()
export class OrderService {
  constructor(private http: Http) {

  }

  fetchOrders(): Observable<OrderModel[]> {
    return this.http.get('https://jsonblob.com/api/68a5b174-309c-11e7-ae4c-55254c90d649')
      .map(r => r.json());
  }

  routeOptimization(data) {
    const a = {
      "service": data,
      "fleet": [{
        "id": 1,
        "lat": 30.7188978,
        "lng": 76.8102981,
        "latEnd": 30.7188978,
        "lngEnd": 76.8102981,
        "returnToStart": 0
      }],
      "maxVisits": 6,
      "polylines": false,
      "distanceCalculation": false,
      "speed": 40,
      "decideFleetSize": 1
    };

    return this.http.post('https://api.flightmap.io/api/v1/vrp', JSON.stringify(a), this.getRequestOptionArgs());
  }

  private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    options.headers.append('Content-Type', 'application/json');
    options.headers.set('Authorization', '772ea600-78ad-11e6-a56b-0bff586a75e5');
    return options;
  }
}
